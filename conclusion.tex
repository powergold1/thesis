\chapter{Conclusion}
\label{chapter:conclusion}

%% what did we do here; what are the strengths and limitations? whar are
%% important questions/challenges.opportunities for the future?

%% - what sets nancy apart is its expansion strategy and backup
%% method.  - data-driven extension to nancy.
\subsubsection{Data-Driven \nancy}
We designed and implemented a data-driven variant of the real-time
search algorithm \nancy\ and evaluated it on several planning
benchmark domains, comparing it against \lsslrta.  An expansion and
backup strategy based on uncertainty and the notion of risk sets
\nancy\ apart from previous real-time algorithms.  Building on the
idea of modeling beliefs about states using probability distributions,
a central concern is how these beliefs are obtained.  The original
version of \nancy\ uses assumptions for this purpose.  While the
evaluation in \tableref{table:solutioncost:expansions} shows that
assumptions can work reasonably well for \nancy, our data-driven
extension, \nancydd, is able to improve search performance
significantly in some domains.

%% no clear measure to indicate how accurate or informative the data
%% really is.  we have only empirically shown that it works or does
%% not work.

Using data to replace assumptions of algorithms is a fairly new
approach and we showed that there may be merit in doing so.  The work
in this thesis was a first step to explore the idea of using data to
assist a planning algorithm during search and enable it to gain more
useful information using limited resources.  While the empirical
results look promising, our approach has only very limited explanatory
potential.  In this thesis, the claims on improved performance are
backed up by experimental results.  Since generating data proved to be
a time consuming task, only six domains were used for evaluation.  It
is not exactly clear how well these results generalize.  The lack of a
formal model to reason about the specific impact of data on the search
makes it hard to reason about the effectiveness of \nancy\ in a more
general sense.  It is therefore not clear, why \nancydd\ only
sometimes improves solution cost compared to \nancy.  Ideally, the
data-driven approach should always be able to obtain more accurate
beliefs which in turn should not result in worse action choices.  The
empirical results are the only point of reference at this stage.

As a result, we can only conjecture why both our advanced data-driven
extensions did not work as well as the more basic variant \nancydd,
which leaves some of the original assumptions in place.  Clearly data
is not the answer to solve all algorithmic decisions.  But it is hard
to tell, which assumptions are suitable to be replaced with data, how
to generate it efficiently, how to make the most use of it, and under
which circumstances assumptions may be the more reasonable choice.

%% (bear mentioning is a cool phrase.  I should use it more often :^)
Another significant drawback of the data-driven approach that bears
mentioning is the required effort in the data generation process.
Generating performance statistics is cumbersome and requires a lot of
work before \nancydd\ is ready to go.  Since the exact impact of the
data being used is not really obvious, there is also no good
indication whether the amount of the generated data is sufficient or
not, and running experiments is currently the only way to test this.

Developing a formal model of the data used to replace certain
assumptions is something that requires further research.  It would
help understand what kind of impact different kinds of data can have
on the search.  This would enable further research in this area to
explore how to make the generation and use of data more effective.

It is an open question whether data generated from one domain may be
used in a whole class of different domains where the heuristic or the
features used to estimate the goal distance exhibit similar kinds of
errors.  The existence of such classes of domains would make the data
generation process more generic, as the data could be reused for all
domains within one class.

On the other hand, the opposite direction of using more specific data
also presents an interesting opportunity.  Using data designed for one
specific state space may make the search more informed.  One such
approach could make use of neural networks.  Previously, neural
networks have been studied as heuristic functions in
\cite{ferber:thesis:2017}, by taking the highest probability output of
the network as the heuristic value.  Using the entire output layer
would yield a probability distribution that could serve as the belief.
It seems promising that more specific data that fits the state space
of interest more closely would make the search more informed and could
therefore further increase performance when one is interested in
solving single instances more efficiently.

% Time bounds
\subsubsection{Time Bounds}
We also extended the real-time planning framework with time bounds as
an alternative to expansion bounds, extending the scope of our
evaluation.  We showed in \tableref{table:nancy:overhead} that the
techniques used in \nancy\ allow for an efficient implementation with
only moderately increased runtime cost compared to \lsslrta, which
often paid off when running under a strict time limit.

Limited use of computationally complex operations in the lookahead and
backup phases is a noteworthy property of the techniques used in
\nancy.  It shows that the tradeoff in complexity between reasoning
about search and actually doing search is worth to keep in mind when
designing real-time search algorithms.  While we were able to show
that \nancy\ can be effective in a true real-time context, it remains
open how it compares against other approaches to real-time search.  In
particular, there are other techniques that have been proposed as
alternative lookahead and backup methods which make use of more
complex operations than \nancy. The Cserna backup rule
\cite{cerna:meta:icaps-17} for example requires computing new
distributions as part of the backup phase, which does not scale well
to large beliefs.  Using the Cserna backup would also prevent the
lookahead phase from using the lazy backup variant as described in
\sectionref{section:lazy}, since the belief at the top level nodes
depends on the beliefs of the intermediate nodes.  However,
Pemberton's \textit{k-best} method described in
\cite{pemberton:ijcai95} and generalized in \cite{ruml:nancy:aaai-19}
offers some hope in that regard.  It interpolates between \nancy-style
backups and Cserna backups limiting the number of expensive Cserna
operations.  Evaluating the effectiveness of these techniques under a
time-bound remains a challenge for the future.

Some algorithms for real-time planning go in a slightly different
direction, making use of other techniques that do not rely on
extensive meta-reasoning.  Time-bounded \astar\ \cite{bjornsson:tba}
is a real-time variant of \astar.  We did not include it in the
evaluation in this thesis due to its limitation to undirected state
spaces.  A thorough comparison of \nancy\ and time-bounded \astar\ is
therefore still open.  Real-time adaptive A* (RTAA*)
\cite{hernandex:aed} is a variant of \lsslrta\ with a simpler backup
rule.  This results in an algorithm which has very low overhead but is
still able to perform well.  If used together with a faster heuristic,
\eg a heuristic based on abstractions, it may be able to do many more
expansions under the same time limit.  How \nancy\ compares against
a simpler, but faster algorithm like RTAA* also remains open.

While we did run \nancy\ under time bounds, we did not consider the
time it takes to execute actions, and instead assumed execution to be
instant.  In a real world implementation however, the actions might
take significantly longer depending on the domain.  When choosing to
execute a longer action, the algorithm receives more time to think
about the action to take after that.  Various methods have been
designed to reason about execution time in the context of concurrent
search and execution, with the intent to make use of the execution
time.  These methods either estimate the utility for choosing an
action \cite{burns:jair:2013} or plan additional time to think
\cite{ruml:socs:2015}.  These suboptimal search algorithms are
designed to minimize goal achievement time.  Hence, there is an
incentive to start execution before a complete plan has been found
just like in real-time planning.  However, the mentioned techniques
are not designed for strict real-time constraints.  Using them in a
real-time algorithm to enable reasoning about action times is still a
new line of research.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
