import numpy as np
import matplotlib.pyplot as plt

d=[1,2,2,3,3,3,3,4,4,5]
s=[2,2.5,2.5,3,3,3,3,3.5,3.5,4]
f=1
bins = [x*(0.5/f) for x in range(1,f*len(d)+2)]
weights= [0.1] * len(d)

fig, axs = plt.subplots(1,2,sharey=True,tight_layout=True,figsize=(7.5,3))
axs[0].hist(d,bins=bins,weights=weights,color="#d43f3a")
axs[1].hist(s,bins=bins,weights=weights,color="#d43f3a")
fig.savefig("./images/squish.png", bbox_inches="tight", dpi=300)
