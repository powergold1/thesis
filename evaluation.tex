\chapter{Evaluation}
\label{chapter:evaluation}

In this chapter we evaluate the described techniques and present the
results.  The evaluation is carried out by running the algorithms presented in this thesis on a number of planning benchmark domains.

We compare assumption-driven \nancy\ with its data-driven variant
\nancydd, including \lsslrta\ as a baseline.  We run a separate set of
experiments to evaluate the effectiveness of the more advanced
variants of \nancydd.  All experiments were run on a cluster of Intel
Xeon E5-2600 machines using the lab framework
\cite{seipp-et-al-misc2017}.  The memory limit was set to 4GB, and the
timeout to 30 minutes in all experiments.

When evaluating real-time planning algorithms, not all domains are
eligible for experiments.
\begin{itemize}
\item Real-time algorithms are never complete if the domain contains
  dead ends or unsolvable fragments.
\item Real-time algorithms are only complete in domains without
  zero-cost actions.
\end{itemize}
We therefore limited ourselves to domains without reachable,
unsolvable fragments, and without zero-cost actions.  For the
Elevators domain in particular, we only included the unit-cost
variant, since the original non-unit cost domain uses zero-cost
actions.  A further limitation for this work specifically is the data
generation process.  Since we needed to generate data based on
training instances of each domain, we limited ourselves to domains
where instance generators were readily available.

We cover three evaluation setups.  To compare the search performance,
solution cost is used as the deciding metric in all of them.  The
first setup compares algorithms under an expansion bound.  We then
take a separate look at additional variants of \nancydd.  The final
setup compares algorithms under a time bound.

\section{Expansion Bound}

In this section we run each algorithm with a bound on the number of
expansions to evaluate the performance of the algorithms with regard
to lookahead size.  This way we compare which expansion strategy is
able to obtain more information with a fixed number of expansions.  By
varying the expansion bound, we show how the search performance of
each algorithm scales with larger lookahead.

\tableref{table:solutioncost:expansions} shows the results of these
experiments.  We compare \lsslrta, assumption-driven \nancy, and
data-driven \nancydd.  We additionally include the original version of
\nancy, whose action selection is non-persistent, as a point of
reference.  We test expansion bounds of 100, 300, 1000.  As a general
trend, solution cost tends to decrease with larger lookahead for each
algorithm.  This is expected, since a larger lookahead makes it
possible to obtain more information before an action choice is due.
Comparing the individual algorithms with each other, we see that both
\nancy\ and \nancydd\ usually outperform \lsslrta, often times quite
significantly.  The original non-persistent variant of \nancy\ is left
behind in most domains though.  \nancydd\ and \nancy\ are very close
on most domains.  \nancydd\ tends to perform better on domains with
action costs, \ie Barman and Transport, while \nancy\ tends to work
better on unit-cost domains.

\setlength{\tabcolsep}{6.5pt}
\begin{table}[h]
\begin{center}\begin{tabular}{l@{\hspace*{3pt}}r|rrrr}
\multirow{2}{*}{Domain}      & \multirow{2}{*}{$L$} & \multirow{2}{*}{\lsslrta}  & \multirow{2}{*}{\shortstack[l]{\ \ \ \ \nancy\\(non-persist.)}}  & \multirow{2}{*}{\nancy} &  \multirow{2}{*}{\ \nancydd} \\
& & & & & \\\hline
\multirow{3}{*}{Barman \numtasks{19}}
        & 100 & \textbf{1239} & 1902 & 1434 & 1342 \\
        & 300 & \textbf{611} & 1113 & 834 & 727 \\
        & 1000 & 559 & 1031 & 702 & \textbf{415}\\ \hline
\multirow{3}{*}{Blocksw. \numtasks{34}}
        & 100  & 45  & 67   & \textbf{39} &  52\\
        & 300  & 42  & 46   & \textbf{36} &  43\\
        & 1000 & 35  & 44   & 39         &  \textbf{34}\\ \hline
\multirow{3}{*}{\shortstack[l]{Elevators \numtasks{25}\\(unit-cost)}}
        & 100  & 50  & 55   & \textbf{35} &  39\\
        & 300  & 32  & 40   & \textbf{29} &  30\\
        & 1000 & 34  & 31   & 27          &  \textbf{26}\\ \hline
\multirow{3}{*}{Parking \numtasks{14}}
        & 100 & 88 & 61 & \textbf{29} & 50\\
        & 300 & 68 & 59 & \textbf{28} & 44\\
        & 1000 & 62 & 45 & \textbf{27} & 31\\ \hline
% \multirow{3}{*}{Termes \numtasks{20}}
%         & 100 & 1225 &  & \textbf{237} & 383\\
%         & 300 & 1437 &  & \textbf{156} & 393\\
%         & 1000 & 662 &  & \textbf{129} & 238\\ \hline
\multirow{3}{*}{Transport \numtasks{17}}
        & 100  & 631 & 1116 & 615         &  \textbf{496}\\
        & 300  & 519 & 705  & 559         &  \textbf{485}\\
        & 1000 & 499 & 607  & 567         &  \textbf{422}\\ \hline
\multirow{3}{*}{\shortstack[l]{Transport \numtasks{23}\\(unit-cost)}}
        & 100  & 48  & 79   & 40          &  \textbf{31}\\
        & 300  & 47  & 43   & \textbf{30} &  34\\
        & 1000 & 35  & 36   & 29          &  \textbf{27}
\end{tabular}
\end{center}
\caption{Geometric means of the solution cost on instances solved by
  all algorithms.  The number of solved instances is given in
  parentheses for each domain.  The limit on the number of expanded
  nodes in the lookahead is denoted by $L$.}
\label{table:solutioncost:expansions}
\end{table}

\section{Further Data-Driven Methods}

This section shows an evaluation of the approaches described in
\sectionref{section:postbelief} and \sectionref{section:morefeatures}
to further increase the extent to which data is used.  $\nancyph$ is a
variant of \nancydd\ which additionally uses the $h$-value of the
parent state as a feature to construct beliefs.  $\nancypost$ is
another variant of \nancydd\ which also constructs the post-expansion
belief $\postbelief$ from data.  Here we evaluate the effect of the
more advanced data-driven techniques compared to \nancydd.

\tableref{table:solutioncost:dd} shows the result of this evaluation.
We see clearly that the most basic algorithm, \nancydd, achieves the lowest
solution cost in almost all cases.  $\nancyph$ was able to improve on
it in very few cases, but is worse most times.  $\nancypost$ is almost
consistently worse than both other approaches.

This evaluation shows that replacing assumptions with data is not as
simple as one would initially hope, as it is not straight-forward how
to generate exactly the right kind of data that will turn out to be
helpful during search.  Some assumptions are harder to generate data
for.

The data that was used as the post-expansion belief in $\nancypost$ is
just a single distribution over $\hstar$-values.  This is an
approximation, and it should rather be a distribution over
distributions to represent different belief outcomes.  However, it is
not clear how \nancy\ would even use this data as a basis for
reasoning.

The data that was used for $\nancyph$ also suffers from certain
issues.  When the data is generated in the offline phase, we record
three values for each state: its $h$-value, the $h$-value of the
parent, and the $\hstar$-value.  There may however be more than one
parent, and we conjecture that the kinds of parent states seen in the
data generation phase are different from the kinds of parent states
seen by \nancy\ during search.

These data-related issues may be the reason why the basic \nancydd\
algorithm works better.

\setlength{\tabcolsep}{6.5pt}
\begin{table}[h]
\begin{center}\begin{tabular}{l@{\hspace*{3pt}}r|rrrr}
\multirow{2}{*}{Domain}      & \multirow{2}{*}{$L$} & \multirow{2}{*}{\nancydd}  & \multirow{2}{*}{$\nancypost$}  & \multirow{2}{*}{$\nancyph$} \\
& & & & \\\hline
\multirow{3}{*}{Barman \numtasks{18}}
        & 100 & \textbf{1437} & 1888 & 2471 \\
        & 300 & \textbf{732} &  809 & 889  \\
        & 1000 & 401 &  387 & \textbf{372}\\ \hline
\multirow{3}{*}{Blocksw. \numtasks{33}}
        & 100 & \textbf{46} &  67 & 47 \\
        & 300 & 42 &  57 &  \textbf{35} \\
        & 1000 & \textbf{32} &  42 &  38 \\ \hline
\multirow{3}{*}{\shortstack[l]{Elevators \numtasks{28}\\(unit-cost)}}
        & 100 &  \textbf{53} & 78 & 70 \\
        & 300 & \textbf{35} & 58 &  41 \\
        & 1000 & \textbf{30} & 40 & 33 \\ \hline
\multirow{3}{*}{Parking \numtasks{10}}
        & 100 &  \textbf{46}&  88&  74\\
        & 300 & \textbf{39}& 46 &  52 \\
        & 1000 & \textbf{26}& 43 & 48 \\ \hline
% \multirow{3}{*}{Termes \numtasks{12}}
%         & 100 &  \textbf{351} & 461 & 479 \\
%         & 300 & 388 &371  & \textbf{338}  \\
%         & 1000 & \textbf{215} & 296 & 273 \\ \hline
\multirow{3}{*}{Transport \numtasks{20}}
        & 100 & \textbf{614} & 790 & 1292 \\
        & 300 & \textbf{576}&  654& 647  \\
        & 1000 & \textbf{427}&  529& 570 \\ \hline
\multirow{3}{*}{\shortstack[l]{Transport \numtasks{25}\\(unit-cost)}}
        & 100 &  \textbf{38} &71 & 92 \\
        & 300 & \textbf{40} &43 &  58 \\
        & 1000 &  \textbf{32} &41 & 48 \\ \hline
\end{tabular}
\end{center}
\caption{Geometric means of the solution cost on instances solved by
  all algorithms.  The limit on the number of expanded nodes in the
  lookahead is denoted by $L$.}
\label{table:solutioncost:dd}
\end{table}

\begin{remark}
  \nancydd's solution costs differ slightly between
  \tableref{table:solutioncost:expansions} and
  \tableref{table:solutioncost:dd} since $\nancypost$ and $\nancyph$
  had slightly lower coverage which affected the average.
\end{remark}

\section{Time Bound}

Since real-time algorithms also need to perform in domains where a
strict deadline has to be met, we also evaluate each algorithm under a
time bound.  This shows whether the overhead of the techniques in each
algorithm pays off in the end.  The comparisons we draw here are of
particular interest to us, considering that \nancy\ does additional
reasoning in the lookahead phase while \lsslrta\ does none.

As the basic \nancydd\ variant works much more reliably than the other
two data-driven variants, we moved forward only considering \nancydd,
excluding the others.

We also exclude the original \nancy\ implementation from time bound
experiments, since its action selection phase is non-persistent.  In
\cite{fickert:gu:staut:aaai20}, \nancy\ is only shown to be complete
with a persistent action selection phase.  This becomes more apparent
when running under time bounds since the lookahead sizes vary
significantly between each search episode.  When different states are
expanded in subsequent lookahead iterations, non-persistent \nancy\
changes direction too frequently causing coverage to drop
significantly.  Including it in the experiments would skew the
averages.  For \nancy\ and all its variants, persistence is a
requirement when running under time bounds.  \lsslrta\ does not have
this issue, since its expansion and backup strategy always lead it to
expand and move in the same direction.

\tableref{table:solutioncost:time} shows the results of our
experiments under time bounds.  We tested time bounds of 100ms, 300ms,
and 1000ms.  They are in fact comparable to the corresponding
expansion bounds since the average time for one expansion is on the
order of 1ms.  Across all domains, the results under time bounds are
similar to expansion bounds.  Both \nancy\ variants usually beat
\lsslrta.  The results look slightly more promising for \nancydd,
which achieves the lowest solution cost in Barman, Parking, and
Transport.  This shows that \nancy's overhead was indeed low enough to
be viable when run under a time bound.

We also note a very slight overall increase in coverage for all
algorithms in our evaluation on time bounds.  This may be due to the
way real-time search scales when more time is available.  As argued in
\sectionref{section:whytime}, larger time limits may enable the
real-time algorithm to push the frontier further, leading to a larger
lookahead, which can be helpful in order to find a goal on larger
instances.

\setlength{\tabcolsep}{6.5pt}
\begin{table}[h]
\begin{center}\begin{tabular}{l@{\hspace*{3pt}}r|rrr}
\multirow{2}{*}{Domain}      & \multirow{2}{*}{$T$} & \multirow{2}{*}{\lsslrta}  & \multirow{2}{*}{\nancy(pers.)} &  \multirow{2}{*}{\ \nancydd} \\
& & & & \\\hline
\multirow{3}{*}{Barman \numtasks{21}}
        & 100ms & 1310 & 1455 & \textbf{1213}\\
        & 300ms & 728 & 867 & \textbf{549}\\
        & 1000ms & 669 & 714 & \textbf{422}\\ \hline
\multirow{3}{*}{Blocksw. \numtasks{35}}
        & 100ms & 54 & \textbf{39} & 43\\
        & 300ms & 50 & \textbf{38} & 39\\
        & 1000ms & 41 & 40 & \textbf{36}\\ \hline
\multirow{3}{*}{\shortstack[l]{Elevators \numtasks{28}\\(unit-cost)}}
        & 100ms & 60 & \textbf{51} & 71\\
        & 300ms & 47 & \textbf{44} & 46\\
        & 1000ms & 38 & \textbf{34} & 39\\ \hline
\multirow{3}{*}{Parking \numtasks{26}}
        & 100ms & 334 & 350 & \textbf{254}\\
        & 300ms & 199 & 156 & \textbf{103}\\
        & 1000ms & 140 & 125 & \textbf{47}\\ \hline
% \multirow{3}{*}{Termes \numtasks{0}}
%         & 100ms & 0 & 0 & 0\\
%         & 300ms & 0 & 0 & 0\\
%         & 1000ms & 0 & 0 & 0\\ \hline
\multirow{3}{*}{Transport \numtasks{23}}
        & 100ms & 919 & 884 & \textbf{739}\\
        & 300ms & 722 & 731 & \textbf{517}\\
        & 1000ms & 664 & 682 & \textbf{500}\\ \hline
\multirow{3}{*}{\shortstack[l]{Transport \numtasks{26}\\(unit-cost)}}
        & 100ms & \textbf{77} & 80 & \textbf{77}\\
        & 300ms & 85 & 58 & \textbf{47}\\
        & 1000ms & 55 & \textbf{42} & 43\\ \hline
\end{tabular}
\end{center}
\caption{Geometric means of the solution cost on instances solved by
  all algorithms.  The time limit is denoted by $T$.}
\label{table:solutioncost:time}
\end{table}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
