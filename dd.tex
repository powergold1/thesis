\chapter{Data-Driven Nancy}
\label{chapter:dd}

How to obtain beliefs about the distance to the goal is a principal
concern of \nancy.  As established in
\sectionref{section:assumptions}, \nancy\ makes use of several
assumptions about heuristic behavior for this purpose.  In this
chapter we describe an alternative approach to obtain these beliefs,
which is based on data.  The idea of this method is to use statistics
about heuristic behavior gathered in an offline phase prior to the
search to construct the beliefs.  Hence, some or all of the
assumptions to construct beliefs at runtime are replaced with data.
Here we cover the details of the data-driven variant of \nancy, which
is also referred to as \nancydd.

\section{Beliefs}
\label{section:beliefs}

When devising a data-driven approach to create beliefs, it is
necessary to consider various aspects of the method used to generate
the data.  These are described in this section.  For illustration
purposes, we have included a number of plots to show the effects of
the data-driven approach.

The purpose of \nancy's assumptions is to obtain a better estimate of
the possible true goal distances $\hstar(s)$ when only $h(s)$ is
available.  To replace these assumptions, we use $h,\hstar$ pairs
yielding a distribution of $\hstar$-values for each $h$-value.

These $h,\hstar$ pairs are generated offline from a number of training
instances.  Each training instance is first solved by an initial
search.  Each state that was expanded in this initial search is then
solved optimally, and its $h$ and $\hstar$-value are recorded.  By
collecting all these pairs, we obtain a set of $\hstar$-values for
each $h$-value, making up the distribution.  To use them during
search, \nancy\ simply looks up the distribution by the $h$-value.

\begin{notation}
  We write $\data$ for the set of all $\hstar$ distributions gathered
  offline.  $\dataof{h}$ denotes the distribution of $\hstar$-values
  for a given $h$-value.
\end{notation}

\begin{example}
  \figureref{fig:blocksviolin} shows the resulting distribution for
  each $h$-value in the Blocksworld domain as violin plots.  The
  values on the $y$-axis are the $h$-values that were seen in the
  training instances, and the $x$-axis shows the corresponding
  distribution of $\hstar$-values.  Each violin is drawn with markers
  for the minimum, the maximum, and the mean.
  \begin{figure}[H] \centering
    \includegraphics[width=0.48\textwidth]{blocksviolin.png}

    \caption{Distribution of $\hstar$-values for each $h$-value in
      Blocksworld}
    \label{fig:blocksviolin}
  \end{figure}
\end{example}

\begin{remark}
  It may happen that we find a state online with an $h$-value that we
  did not encounter in the offline training.  In that case, we perform
  an online extrapolation step on the data.  We take the largest
  $h' \leq h$ for which we have a distribution in the data set.
  $\dataof{h}$ is extrapolated by shifting $\dataof{h'}$ by the
  difference between $h$ and $h'$.  The newly created distribution is
  cached to have it available in the future.  That is, we compute and
  store a new distribution
  $\dataof{h} \coloneqq \dataof{h'} + (h-h')$.
\end{remark}

%%\subsubsection{Beliefs by Domain}

In order to make the data representative of the heuristic behavior, it
is generated for each domain individually, which requires additional
work for each new domain \nancydd\ is intended to run on.  This is
however necessary, since heuristic behavior can vary a lot between
domains.  Therefore, generating new data for each domain results in
more informative distributions.  The difference in heuristic behavior
can be seen when comparing the resulting beliefs for different
domains.

\begin{figure}[H]
  \centering
  \begin{minipage}{0.5\linewidth}
  %% \ \vspace{-22.5em}
    \centering
    \includegraphics[width=0.85\textwidth]{blocksvtu_b.png}
  \end{minipage}%
  \begin{minipage}{0.5\linewidth}
    \centering
    \includegraphics[width=0.85\textwidth]{blocksvtu_tu.png}
  \end{minipage}

  \caption{Beliefs generated for Blocksworld and Transport (unit-cost).}
  \label{fig:blocksvtu}
\end{figure}

\figureref{fig:blocksvtu} shows the different $\hstar$ distributions
for Blocksworld and unit-cost Transport side by side.  Comparing the
distributions for both domains, we notice several points:
\begin{itemize}
\item In Blocksworld, the initial search found states with slightly
  larger $h$-value.
  \item  The spread of possible $\hstar$ values is greater in unit-cost Transport.
  \item The expected value increases monotonically in both domains,
    but slightly faster in unit-cost Transport.
\end{itemize}

\newpage
%%\subsubsection{Beliefs for Domains with Action Costs}

Whether a domain has non-unit action costs has an obvious dramatic
effect on the observed data.  \figureref{fig:elevvunit} shows the
resulting distributions for a unit-cost variant of Elevators and its
original variant with action costs.

\begin{figure}[H]
  \centering
  \begin{minipage}{0.5\linewidth}
  %% \ \vspace{-22.5em}
    \centering
    \includegraphics[width=0.8\textwidth]{elevvunit_u.png}
  \end{minipage}%
  \begin{minipage}{0.5\linewidth}
    \centering
    \includegraphics[width=0.8\textwidth]{elevvunit_e.png}
  \end{minipage}

  \caption{Beliefs in unit-cost Elevators and beliefs in Elevators
    with action costs.}
  \label{fig:elevvunit}
\end{figure}

\newpage
%%\subsubsection{Collecting Representative States}

Recall that the set of states that are solved optimally to create
these distributions is determined by the states seen in the initial
search.  In fact, the only purpose of the initial search is to collect
these example states.  There are two reasons for solving only a small
set of states instead of the entire state space.  The first reason is
practical feasibility.  For instances of a certain size, solving the
entire state space would require an unreasonable amount of memory and
time.  The second reason is that solving the entire state space is
unlikely to yield the kind of data we are interested in.  We want to
use the data to inform the algorithm how the heuristic typically
behaves on a state.  However, a large state space contains many states
that are never seen in an actual search.  By only collecting states
that are seen in a search, we aim to make the data more
representative.

However, this leads to the question which initial search algorithm to
use to collect example states.  Ideally the set of states should be
representative for the kind of states seen by \nancydd\ online, since
this would provide \nancydd\ with more accurate goal distance
estimates.

Since \nancydd\ is a suboptimal search algorithm, we use another
suboptimal search algorithm for this purpose.  We have chosen \wastar\
with a weight of $2$, and \lsslrta\ as two examples.
\figureref{fig:ldatacmp} shows the resulting distributions for both
methods.  Comparing them, we observe that they are very similar to
each other.  The similarity of the beliefs generated by the two
algorithms is not too surprising.  The two algorithms expand states
differently and in a different order which may lead to a different set
of states that were visited.  However, we conjecture it is unlikely
for them to observe different kinds of heuristic behavior over all
states.

\begin{figure}[H]
  \centering
  \begin{minipage}{0.5\linewidth}
  %% \ \vspace{-22.5em}
    \centering
    \includegraphics[width=0.8\textwidth]{wavl_wa.png}
  \end{minipage}%
  \begin{minipage}{0.5\linewidth}
    \centering
    \includegraphics[width=0.8\textwidth]{wavl_l.png}
  \end{minipage}

  \caption{Beliefs in Blocksworld generated with \wastar\ and
    \lsslrta.}
  \label{fig:ldatacmp}
\end{figure}


\begin{remark}
  A somewhat idealistic approach, that one could imagine, to generate
  the most representative data would perform multiple data generation
  runs with \nancydd\ itself as the initial search, to bootstrap and
  upgrade its own beliefs until a fixed point is reached.  However,
  such a procedure is out of scope here, since it is not clear whether
  such a fixed point even exists.
\end{remark}

% \begin{remark}
%   Our method to generate pairs of $h$ and $\hstar$ depends on which
%   states were visited in the initial search.  What kind of states are
%   seen by the search depends on the training instances.  Since there
%   are of course many more instances than we can train on in a
%   reasonable amount of time, we cannot know how closely the
%   distribution of $\hstar$-values for each $h$-value matches the true
%   distribution if all possible instances were taken into account.
% \end{remark}

%%\subsubsection{Number of Training Instances}

A further key concern for any data-driven algorithm is how to
determine the number of examples necessary to ensure that the data
seen in those examples is representative for the general case.  This
is of special importance with our setup, since we only consider a
subset of all states for any given example instance.  As an empirical
indication whether the amount of data we gathered is sufficient, we
then the number of sample states by a factor of 2 and compared the
resulting beliefs.

\begin{figure}[H]
  \centering
  \begin{minipage}{0.5\linewidth}
  %% \ \vspace{-22.5em}
    \centering
    \includegraphics[width=0.85\textwidth]{r2cmp_b.png}
  \end{minipage}%
  \begin{minipage}{0.5\linewidth}
    \centering
    \includegraphics[width=0.85\textwidth]{r2cmp_r2.png}
  \end{minipage}

  \caption{Beliefs in Blocksworld generated on 35 examples instances
    and on 17. }
  \label{fig:r2cmp}
\end{figure}

\figureref{fig:r2cmp} shows the two sets of distributions side by side.
As expected, the spread of $\hstar$-values is smaller when reducing
the number of sample states.  The overall trend however is the same
for both distributions.

\newpage
\section{Post-Expansion Beliefs}
\label{section:postbelief}

A further assumption used in the original formulation of \nancy\ is
the estimate how the belief of a state $\beliefof{s}$ would change
with further search.  The proposed method assumes $\hhat(s)$ to stay
the same, while the variance of the distribution is reduced by a
factor depending on the estimated number of steps to the goal.

This assumption is motivated by the fact that heuristics tend to be
more accurate when very close to the goal. However, this is a very
rough generalization.  The amount by which variance is assumed to
decrease in \nancy\ is based on the number of steps to the goal, which
is just another estimate.

Since estimating the change in $\beliefof{s}$ we would observe if we
expanded the state without actually expanding it is very difficult and
requires making use of strong assumptions, we study the use of data as
an alternative method to obtain this estimate.

The way to generate the necessary data is an extension of the method
used in \sectionref{section:beliefs}.  For all states that were solved
optimally to obtain the $h,\hstar$ pair, we additionally consider all
applicable actions $a \in A(s)$.  For each successor $\succof{s}{a}$
with heuristic value $\hof{\succof{s}{a}}$, we already have a
distribution of $\hstar$ values: $\dataof{\hof{\succof{s}{a}}}$.
Since the change of the belief $\beliefof{s}$ would be obtained by a
backup from the cheapest successor, we keep only the distribution with
lowest expected value.  Combining all distributions for states of the
same $h$-value yields a distribution representing which
$\hstar$-values we are expected to see after expanding a state of this
$h$-value.

\begin{algorithm}[h]
  \caption{$\postdata$ Data Generation}
  \label{alg:postdata:generation}
  \DontPrintSemicolon
  \SetKwData{Left}{left}
  \SetKwData{This}{this}
  \SetKwData{Up}{up}
  \SetAlgoNoEnd

  \SetKwInOut{Input}{Input}
  \SetKwInOut{Output}{Output}

  \Input{solved : Set of states that were solved in the initial search\\
         $\data$\ : Distribution of $\hstar$ values for each $h$}
  \Output{Distribution representing the post-expansion belief}
  \BlankLine
  $\postdata \coloneqq \{ \}$ \;
  \For{s in solved}{
    $\textit{cheapest} \coloneqq \argmin\limits_{a \in \applicables{s}}(\hhatof{\dataof{\hof{\succof{s}{a}}}})$ \;
    $\textit{succ} \coloneqq \succof{s}{\textit{cheapest}}$ \;
    $\postdataof{h(s)} \coloneqq \postdataof{h(s)} \cup (\dataof{\hof{\textit{cheapest}}} + c(\textit{cheapest}))$ \;
  }
  \Return $\postdata$
\end{algorithm}

\newpage

\figureref{fig:postbelief} shows the beliefs in the Blocksworld domain
and the corresponding post-expansion beliefs generated by running
\algorithmref{alg:postdata:generation}.  It shows that the expected
value is generally the same for both sets of distributions.  As
opposed to the assumption made by \nancy\ however, variance is not
necessarily decreased.  The spread of possible $\hstar$ values is
slightly larger in the distributions for the post-expansion belief.

\begin{figure}[h]
  \centering
    \begin{minipage}{0.5\linewidth}
    \centering
    \includegraphics[width=0.85\textwidth]{postbelief_orig.png}
  \end{minipage}%
  \begin{minipage}{0.5\linewidth}
    \centering
    \includegraphics[width=0.85\textwidth]{postbelief_post.png}
  \end{minipage}
  \caption{Belief in Blocksworld and the corresponding post-expansion
    belief}
  \label{fig:postbelief}
\end{figure}

\newpage
\section{Additional Features}
\label{section:morefeatures}

% When using an algorithm like \nancy\ which uses probability
% distributions to model its own uncertainty, it is important that these
% distributions describe the possible outcomes as closely as possible.
% However, the heuristic is so far the main source of information during
% search which just yields a single scalar value that is mapped to a
% distribution.  It is therefore a valid question to ask whether the
% resulting distribution does fulfill its purpose, since we assign the
% exact same belief to all states of the same $h$-value.

% The data-driven method to generate beliefs provides an opportunity to
% improve on this issue by extending the way beliefs are assigned to
% states.  The idea is to use other features of the states.  When the
% beliefs are generated offline, these features are recorded in addition
% to the $h$-value.  This way we get a separate distribution of $\hstar$
% values for each feature vector.  During the search, the beliefs are
% then not just looked up by $h$, but by the entire feature vector of
% the state instead.


Ideally, the belief of each state contains a set of possible $\hstar$
values that are representative for this state, since the belief is
used to determine which node requires further search.  Other than in
classical planning, it is not necessarily required to have a
relatively exact goal estimate, close to the true value.  In real-time
planning, the true goal distances are impossible to obtain anyway due
to the time limit.  Instead, the distribution of possible goal
distances ought to represent a range of goal distances that are
reasonable to expect, and to enable further reasoning about this
belief.  It is therefore desirable to have beliefs that inform the
search how certain we are in our beliefs.  In order to achieve a
degree of certainty about the goal distance, the beliefs should not be
too general.

Considering our data-driven approach, this means it is undesirable to
aggregate too many states in the same bucket, since the resulting
distribution will contain all of their possible outcomes.  A
distribution may for example contain states of some $h$-value that are
in a local minimum, \ie the heuristic underestimated, and states that
are close to the goal, where the heuristic value is more accurate.  By
collecting both kind states in the same distribution, the resulting
belief is representative for neither of them.  We therefore want to be
able to distinguish the states further to make the offline generated
data more informed and the beliefs that are looked up during search
more specific.

One approach for this is to use more features to distinguish different
kinds of states.  Instead of just using $h$ and mapping it to a
distribution, any arbitrary set of properties of the state that is
known during search may be used when querying the belief, e.g. the
number of successors, the $h$-value of the parent, or a second
heuristic value $h_2$.  This way, we split up the each ``bucket'' of
possible $\hstar$-values into several different ones, increasing the
chance that the belief is appropriate for the state we looked it up
from.

We implemented the $h$-value of the parent as an example of an
additional feature as it was the most straightforward to add to the
existing algorithm.  In this case, $\data$ is a mapping from a pair of
heuristic values, from the current and the parent state, to a
distribution.

\begin{figure}[h]
  \centering
  \begin{minipage}{0.5\linewidth}
  %% \ \vspace{-22.5em}
    \centering
    \includegraphics[width=0.85\textwidth]{phdata_1.png}
  \end{minipage}%
  \begin{minipage}{0.5\linewidth}
    \centering
    \includegraphics[width=0.85\textwidth]{phdata_2.png}
  \end{minipage}
  \caption{Beliefs in Blocksworld, generated for pairs $(h, h_p)$,
    where $h$ is the heuristic value of the current state, and $h_p$
    is the heuristic value of the parent.}
  \label{fig:phdata}
\end{figure}

\figureref{fig:phdata} shows the resulting distributions for this
approach.  In order to show all distributions, the plot is made of two
halves, one for the smaller $h$-values and one for the larger.  It can
be seen that the distributions for a given $h$-value do indeed differ
for different values of $h_p$.  The distributions for $h=12$ for
example are differently shaped for each value $h_p \in \{ 11,12,13\}$.
However, we already observe a drawback of this approach.  Since we
divide up the states that were seen during search even further, some
rare $(h, h_p)$ configurations like $(9,11)$, or $(20,22)$ were
observed only once.  These distributions degenerate to a single sample
point, which \nancy\ would mistakenly interpret as being perfectly
certain about this value.  There are possible ways to overcome this
issue of data scarcity in the data generation process.

One is to collect more data until each feature was observed a minimum
number of times.  This approach turns out to be rather difficult in
practice.  We tried generating data with twice the number of training
instances and observed that it did fill up some of the previously
scarce feature pairs with more data.  However, it also introduced new
feature pairs with scarce data that were not found before at all.  In
reality, there is no clear guarantee that this approach is able to
eventually fill up all possible feature combinations with a sufficient
amount of data.
\newpage
An alternative to generating more data, is to interpolate between
other features for which we already have more data.  This might reduce
some of the benefits of using additional features, since merging
feature buckets would undo their clear separation, but it would
guarantee that a rare feature does not result in a falsely interpreted
belief.

However, in order to not further complicate our use of additional
features, we accepted the scarcity for some features as a side effect.
Since features that are rarely observed during offline training are
unlikely to be seen very often during an actual search, their
potential to do harm is very limited anyway.



One further point needs to be considered when using more features than
just $h$.  Recall that we extrapolate from existing data, whenever we
are missing exact data, that is, whenever we find a state during the
search with a feature configuration we did not encounter during
training.  The more features are used, the more likely it is to run
into this case.  Additionally, the extrapolation procedure is more
complex for multiple features, since we need to search for the overall
best match for any given feature vector.  We implemented a general
extrapolation method for this purpose, which first extrapolates along
$h$, and then looks for the overall closest matching feature vector.
% Given an arbitrary feature vector $v$, we first look for the largest
% $h' < h$ for which we have data in $\data$.  Among all feature
% vectors for $h'$, we choose the feature vector $v'$ which matches
% $v$ most closely.
The reason, we prioritize the $h$-value when extrapolating is that it
is the feature that describes the state most closely, and also
determines by how much the extrapolated distribution should be
shifted.  In the following we write $v$ for an arbitrary feature
vector for some state $s$ which is assumed to contain at least
$\hof{s}$.  The value $\hof{s}$ in $v$ is denoted by $\texttt{h}(v)$.

\begin{algorithm}[h]
  \caption{Data Extrapolation}
  \label{alg:data:extrapolation}
  \DontPrintSemicolon
  \SetKwData{Left}{left}
  \SetKwData{This}{this}
  \SetKwData{Up}{up}
  \SetKwFunction{h}{h}
  \SetAlgoNoEnd

  \SetKwInOut{Input}{Input}
  \SetKwInOut{Output}{Output}

  \Input{$v$ : feature vector}
  \Output{$\belief$ : belief for $v$}
  \BlankLine
  \If{$v$ in $\data$}{
    \Return $\dataof{v}$
  }
  $h' \coloneqq \h(v)$\;
  \While{true \label{line:extrapolation:loop}}{
    \If{$\exists v' \in \data,\ s.t.\ \h(v') = h'$}{
      $\textit{chosen} \coloneqq \argmin\limits_{v' \in \data,\ \h(v') = h'}(\left\lVert v - v' \right\rVert)$ \;
      $\dataof{v} \coloneqq \dataof{\textit{chosen}} + (h - h')$ \;
      \Return $\dataof{v}$ \;
    }
    $h' \coloneqq h'-1$\;
  }

\end{algorithm}

\algorithmref{alg:data:extrapolation} shows how the extrapolation is
carried out.  An exact match is always preferred, otherwise we take
the closest match for the largest $h' \leq h$, where data exists for
$h'$, and extrapolate from it.  Note that the loop
\algolineref{line:extrapolation:loop} terminates when data for any
$h'$ is available.  $\data$ is assumed to contain at least one
distribution for a goal state, \ie any feature vector $v$ with
$\texttt{h}(v) = 0$.  Hence, \algorithmref{alg:data:extrapolation}
eventually terminates if called with feature vectors $v$ with
$\texttt{h}(v) \geq 0$.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
