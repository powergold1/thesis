\chapter{Implementation}
\label{chapter:implementation}

In this chapter we give details on the implementation of \nancy.
Since \nancy\ is a real-time algorithm, and we evaluate \nancy's
effectiveness also in the presence of time bounds, several
implementation details play an important role.  The overhead
introduced by the additional reasoning in the lookahead phase and the
use of probability distributions instead of single scalar values
requires some care to make the algorithm run smoothly and to avoid
bottlenecks in the execution.

The implementation is done in the well-established Fast-Downward
planning framework \cite{helmert:jair-06}.  Fickert originally
implemented a plugin for expansion-bound real-time algorithms,
specifically \lsslrta.  The contribution of this thesis is the
implementation of time bounds as an alternative to expansion bounds,
as well as the implementation of \nancy\ and \nancydd.  We use a
belief data structure for \nancy\ which was originally based on
Mitchell's implementation in \cite{mitchell:thesis:2018}, but has
since been replaced with a more memory-efficient alternative.  The
entire project is 5042 lines of C++ code.

\section{Lazy Backup}
\label{section:lazy}

%   The backup method used is a
% \emph{lazy} variant of the full \nancy\ backup.  The lazy backup only
% updates the beliefs at the root, but skips all intermediate nodes
% reducing the number of necessary backup steps significantly.

% Maybe I should go about this as I did before (I removed what I wrote
% though): Talk about the two concerns of the backup, 1. update the
% top level beliefs in each lookahead iteration, to update the view of
% the top level actions. 2. update the intermediate beliefs to make
% the information gathered in the lookahead available in future
% iterations.

Backing up the beliefs from the frontier to the top level nodes, is a
necessary operation to reflect the algorithms updated view of the top
level actions.  The updated beliefs at the top level are used in the
lookahead phase each time the planner chooses the direction in which
to expand next.  \algorithmref{alg:nancy:backup} which is used to
perform these updates on the local search space however is quite
expensive to perform in a loop.  It is a variant of the update method
used by \lsslrta, which is shown by Hernández and Baier in
\cite{hernandex:aed} to have a complexity of $\bigo{\log(n)*n}$, where $n$
is the number of states in the local search space.  The complexity of
the backup therefore scales with the number of lookahead iterations as
successive iterations add more states that the backup needs to account
for.

However, we observe that the backup in \nancy\ takes care of two
separate concerns.  Firstly, it updates the beliefs in the local
search space to learn them for future search episodes. This kind of
update is necessary to escape local minima and prevent running in
cycles.  The second concern is the update of the beliefs of the top
level nodes in each lookahead iteration.  Since the top level beliefs
are used to choose the direction of lookahead, and determined by their
successors, they require an update before every lookahead iteration.

\newpage

By separating these two concerns, a simpler backup may be used during
the lookahead phase.  The lookahead phase only requires that the
beliefs at the top level are updated, since the direction of lookahead
is chosen exclusively at the top level.  Furthermore, \nancy\ always
backs up the belief of the best successor to the parent, shifted by
the action cost.  Propagating this to the top level, the belief for
each the top level node is completely determined by the belief of the
cheapest frontier node.  We can therefore use a simplified backup
method in the lookahead phase which only updates the top level
beliefs.

\begin{algorithm}[h]
  \caption{\nancy\ backup (lazy)}
  \label{alg:lazy}
  \DontPrintSemicolon
  \SetKwData{Left}{left}
  \SetKwData{This}{this}
  \SetKwData{Up}{up}
  \SetAlgoNoEnd

  \SetKwInOut{Input}{Input}
  \SetKwInOut{Output}{Output}

  \Input{lss : local search space}
  \BlankLine
  \For{tla in TLAs} {
    $\beliefof{s\llbracket tla \rrbracket} \coloneqq \beliefof{tla.open.min} + g(tla.open.min)$\;
    $\postbeliefof{s\llbracket tla \rrbracket} \coloneqq \postbeliefof{tla.open.min} + g(tla.open.min)$\;
  }

\end{algorithm}

Since \algorithmref{alg:lazy} only updates the beliefs at the top
level, its complexity is $\bigo{m}$, where $m$ is the number of top
level actions.  More notably, it is constant in the size of the
lookahead space, so its complexity does not scale with additional
nodes that are added to the local search space.  Skipping all
intermediate nodes in the local search space means they are left
unaligned with their successors for the remainder of the current
lookahead phase.  These are instead updated by the full backup which
is only applied once at the end of the lookahead phase.

\section{Belief Representation}

One main idea driving Nancy is its model of the algorithm's own
uncertainty using probability distributions over goal distances.
Since they are used extensively in the lookahead phase to decide where
to do more search, it is crucial to reduce their computational
overhead as much as possible, to make the additional reasoning worth
it.

When designing the belief data structure, it is important to consider
how the values are accessed during search, and which operations are
needed.  In \nancy, we note the following:
\begin{itemize}
\item For each state $s$, its belief $\beliefof{s}$ is only
  constructed once and then cached for all future iterations.
\item A single $h$ always maps to the same distribution.
\item The belief $\beliefof{s}$ does not change outside of the learning
  phase for any state $s$.
\item The only modification to the initial distribution is a shift
  during the learning phase when beliefs are backed up from the
  successors of a state.
\end{itemize}

Combining these points we arrive at a few very specific requirements.
We now describe how they are implemented and show that the approach
enables a sufficiently efficient belief backup.

\begin{remark}
  To simplify the explanation, we discuss how $h$ is mapped to a
  distribution.  However, the approach generalizes to arbitrary
  feature vectors $v$.
\end{remark}

Since each $h$ always maps to the same distribution, we create the
distribution $\dataof{h}$ only once and store it.  This is already
done during initialization, so that the distribution for each $h$ is
available already.  When a new state $s$ is encountered in the actual
search, we compute $\hof{s}$, look up the corresponding distribution
$\dataof{\hof{s}}$, and give $s$ a reference to it.  This way,
\nancydd\ does not need to construct entirely new distributions at
runtime.  The only exception to this is when we need to extrapolate,
which cannot be avoided in general, but only happens very rarely.

Furthermore, we let all states of the same heuristic value share a
single distribution, reducing the memory overhead.  The only time the
belief of a state $\beliefof{s}$ can change is during the learning
phase, where each state is assigned the belief of its best successor,
shifted by the cost of the action to reach it.  Since we let many
states share the same distribution, we must not modify the sample
points directly.  Instead, we store the amount by which the belief is
shifted separately for each state.  $\beliefof{s}$ is therefore a pair
$(\dataof{\hof{s}}, \textit{shift}(s))$, \ie a reference to a
distribution and a scalar shift.  This way, the distribution shared by
all states only serves as a base, while all per-state belief updates
are stored compactly as a single scalar value.

To backup the belief from a child to its parent, we only update the
pointer to the base distribution and add the cost of the action to the
shift.  More formally, the backup of the belief from $\succof{s}{a}$
to its parent $s$ is performed as follows:
\[
  \beliefof{s} \coloneqq (\dataof{\hof{\succof{s}{a}}},
  \textit{shift}(\succof{s}{a}) + c(a))
\]

Representing beliefs as a distribution and a separate shift amount has
two concrete benefits.  Firstly, we only backup a pointer and an
integer, instead of backing up a whole distribution of many sample
points.  Secondly, we only update a single number to change the shift,
instead of creating a whole new distribution.  Both these points make
the belief backup in the learning phase almost as efficient as the
scalar backup used by \lsslrta.

The only other consequence of this representation is that the cost
values of the distribution within a belief cannot be accessed
directly.  Instead, the shift needs to be added to the sample values
when they are used in \algorithmref{alg:nancy:risk} to compute the
risk for a given belief configuration.

\section{Belief Size}

In \nancydd, the offline generated data is loaded when initializing
the search.  The input data may however be arbitrarily large.  This
can cause unexpected bottlenecks in the computation of risk,
\algorithmref{alg:nancy:risk}.  Since we are integrating over pairs of
sample points once for each top level action, the procedure is very
sensitive to the number of sample points in each distribution.
Distributions with a large number of different sample points may cause
one lookahead iteration to take much longer than another, which makes
it harder to estimate precisely when \nancy\ should stop the lookahead
while running under a time limit.

It is therefore undesirable for a distribution to contain a wide range
of possible $\hstar$-values.  For most domains, the generated
distributions have very few sample points, but the spread of
$\hstar$-values is generally larger for domains with action costs like
Transport.  To prevent any sudden overhead in those domains, we limit
the number of sample points in each distribution to a reasonably small
constant.  We set the limit to $64$ as an arbitrary choice, which is
more than enough for most domains, but also not too restrictive for
domains with wider distributions.  If there are more sample points in
a distribution, we apply a method to down-sample it.  This is done by
iterating over all sample points, aggregating those next to each other,
until a minimum threshold of probability mass has been accumulated.
The value of the filter determines the maximum number of sample points
in the down-sampled distribution.

\begin{algorithm}[h]
  \DontPrintSemicolon
  \SetKwData{Left}{left}
  \SetKwData{This}{this}
  \SetKwData{Up}{up}
  \SetAlgoNoEnd

  \SetKwInOut{Input}{Input}
  \SetKwInOut{Output}{Output}

  \Input{$d$ : probability distribution}
  \Output{$d'$ : down-sampled probability distribution}
  \BlankLine
  $\textit{filter} \coloneqq 1 / 64$ \;
  $hs \coloneqq 0$ \;
  $\textit{mass} \coloneqq 0$ \;
  $d' \coloneqq \{ \}$ \;
  \For{$\textit{value}, \textit{probability} \in d$} {
    $hs \pluseq \textit{value} * \textit{probability}$ \;
    $\textit{mass} \pluseq \textit{probability}$ \;
    \If {\textit{mass} $\geq$ \textit{filter}} {
      $\displaystyle d' \coloneqq d' \cup \{(\frac{hs}{\textit{mass}}, \textit{mass})\}$ \;
      $hs \coloneqq 0$ \;
      $\textit{mass} \coloneqq 0$ \;
    }
  }
  \Return $d'$\;
  \caption{$\data$ Down-sampling}
  \label{alg:downsample}
\end{algorithm}


\section{Risk Computation}

Risk is the expected regret in the case that the top level action with
the currently lowest expected cost turns out to be worse than one of
the others.  Intuitively, this is the overlap between the belief for
the currently best top level node $\alpha$ and all alternatives
$\beta_i$.  Here we consider the overhead introduced by the risk
computation in more detail.

Recall that \algorithmref{alg:nancy:risk} computes the risk value by
integrating over the samples points in the belief distributions of
$\alpha$ and the samples points in the belief distribution of
$\beta_i$.  We add up the cost for all those pairs of samples points
where the value from $\alpha$ is more expensive than a value from
$\beta_i$.

The complexity of this operation is non-trivial.  Let $m$ be the
number of top level actions, and $k$ the maximum number of sample
points in the beliefs of all $m$ top level actions.  Then iterating
over the pairs of sample points for all belief distributions has a
complexity of $\mathcal{O}(mk^2)$.

In the lookahead phase, we choose to expand under the top level action
of minimal risk.  For each top level node, we estimate $\postbelief$
and compute its risk value.  The complexity to select the top level
action to expand under is therefore $\mathcal{O}(m^2k^2)$.

Selecting the top level action to expand under is the very first step
in each iteration of the lookahead phase.  It is the single part where
\nancy\ does more work than \lsslrta.  However, even though the
operation is of quadratic complexity in two parameters, it turns out
not to be a computational bottleneck.

\setlength{\tabcolsep}{6.5pt}
\begin{table}[h]
\begin{center}\begin{tabular}{l@{\hspace*{3pt}}r}
\multirow{2}{*}{Overhead}      & \multirow{2}{*}{Function} \\
\\\hline\\
$92.22\%$ & $\lmcut$ \\
$1.57\%$ & risk computation \\
$\vdots$ &  \\
$0.08\%$ & belief backup \\
$0.05\%$ & lazy backup \\
$0.01\%$ & action selection \\
$\vdots$ &
\end{tabular}
\end{center}
\caption{Average relative overhead in \nancy.}
\label{table:nancy:overhead}
\end{table}

\tableref{table:nancy:overhead} shows relative amounts of time and
where it was spent.  \nancy\ spends the vast majority of its time
computing heuristic values, while the risk computation makes up less
than $2\%$.  The backup and action selection phases take only a
negligible amount of time.  To focus just on the overhead introduced
by \nancy, we have omitted other entries that were below $1\%$.  These
were miscellaneous other function calls in Fast Downward.

Since computing $\lmcut$ is the obvious bottleneck, and both \lsslrta\
and \nancy\ need to go through its computation, neither of them is
likely to have a significant advantage over the other.  This is
confirmed when evaluating \nancy\ and \lsslrta\ under time bounds in
\chapterref{chapter:evaluation}.

There are a number of reasons why the overhead of the risk computation
is relatively small in practice:

\begin{itemize}
\item The inner loop consists of only a few instructions; a
  multiply and add, or a jump to the exit of the loop.
\item The number of sample points within a distribution is limited by
  a relatively small constant of 64.  Depending on the domain however,
  the number of sample points per distribution is well below this
  limit.
\item Compiling with -O3, the loop is unrolled, and since the beliefs
  are stored in contiguous memory, simd instructions are used within
  the unrolled loop body (depending on the CPU architecture).  This
  way, a large part of the loop body turns into straight line code.
\end{itemize}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
