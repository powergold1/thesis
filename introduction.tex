\chapter{Introduction}
\label{chapter:introduction}

Planning is a branch of artificial intelligence concerned with finding
action sequences leading to a goal state of a planning task.  In
classical planning, a complete plan needs to be found before execution
is started.  However, artificial intelligence techniques have become
increasingly popular in environments where responsiveness is
essential, \eg intelligent user interfaces or soft real time systems
like video games.  In those domains, it is undesirable for the system
to take as much time as necessary to find a full plan and to start
acting.

Real-time planning, introduced by Korf in \cite{korf:real-time:1990},
addresses this issue.  It is an alternative paradigm for planning
algorithms with the defining trait that the delay between planning and
execution is minimized by committing to the next action within a
strict time limit.  The objective of real-time planning is to minimize
total execution time, while providing two main benefits.  Firstly,
since the first action needs to be chosen after a short amount of
time, real-time planning algorithms are able to start execution very
quickly.  Secondly, real-time planning algorithms have the capability
of performing planning and execution concurrently.  In the real world,
the real-time system would execute the action immediately after
committing to it, while planning the next one at the same time.  This
could turn out to be beneficial for the total time needed to reach the
goal compared to the classical approach.  Since the classical approach
needs to find a complete plan to the goal first, a suboptimal action
sequence executed in real-time might reach the goal faster than an
optimal plan found by the classical approach.

There are some inherent limitations of real-time planning due to the
time constraint which puts an upper bound on the amount of search the
agent may do.  Firstly, optimality of the selected action cannot be
guaranteed.  The agent may even select an action that leads away from
the goal.  Secondly, the agent cannot detect dead ends reliably.  As
pointed out in \cite{burns:jair:2013}, real-time search is generally
not applicable in domains where a wrong action could cause damage or
system failure.  Hence real-time search algorithms are suboptimal
search algorithms limited domains without dead ends.

Ever since their inception in \cite{korf:real-time:1990}, real-time
planning algorithms have followed an approach composed of three
phases.  Firstly, the agent does some limited amount of lookahead to
create a local search space.  Then it estimates the remaining cost to
the goal for each possible action by making use of the information
obtained during the lookahead.  Lastly, the agent commits to the
action leading to the lowest estimate and executes it.  To prevent
running in circles, the goal distance estimates are updated for all
states in the previously expanded space.  By repeating this process to
select the next action, the system continues execution, incrementally
working towards a goal.

The three-phase approach leaves some details open for each phase:
\begin{enumerate}
\item Lookahead: Which nodes to expand?
\item Selection: In order to choose the next action to execute, how to
  estimate the remaining goal distance for each action applicable at
  the top level?
\item Learning: How to update the heuristic values in the search
  space?
\end{enumerate}

The \lsslrta\ algorithm \cite{koenig-sun:lsslrta:2009} is one of the
most popular instantiations of the three-phase approach.  In the
lookahead phase, the well-known \astar\ algorithm \cite{Hart1968} is
used, always expanding the node with the smallest $f$-value.  The goal
distance estimates at the top level are given by the minimum $f$-value
at the frontier of the local search space.  The learning phase updates
the $h$-values by performing a dynamic programming-like algorithm,
assigning each state the $h$-value of the cheapest successor shifted
by the action cost.

While \lsslrta\ has become the standard example of real-time search
algorithms, several problems have been identified regarding its
decision making arising from the time limit on the search.  In the
first phase, the expansion strategy only looks at a state's $f$-value.
Following the smallest $f$-value is however likely not of much use for
the search.  Unless the heuristic changes drastically below the node
that is expanded, the expansion is unlikely to change the agent's
belief about which action to choose, \ie it is unlikely to reveal more
information.  The underlying issue is that classical \astar\ wants to
find a complete plan and hence searches in the direction that seems
closest to the goal.  In real-time planning however, the purpose of
search is to select just the next action.  With this in mind, more
useful information could be obtained by considering to expand other
search nodes closer to the top level to make a more informed action
choice.

Furthermore, selecting the next action based only on the minimum
$f$-value of its successors means the agent relies a lot on a single
seemingly good value and ignores all other successors.  Pemberton and
Korf observed in \cite{pemberton:korf:aaai94} however, that the action
towards the smallest $f$-value is not necessarily the optimal choice.
An important consideration for real-time algorithms is therefore,
whether to choose the action with a single successor of minimum
$f$-value or to take the safer option with multiple successors of
slightly higher $f$-values.

These issues have lead to the realization that imposing a time limit
on the search creates unique challenges for real-time planning as a
paradigm, which algorithms in this category have to face.  These
challenges arise due to the uncertainty under which real-time
algorithms have to perform.  Search strategies established in the
classical setting do not necessarily continue to be optimal in the new
context, which motivates considering new alternative approaches.

One such approach is shown by Spaniol in \cite{spaniol:thesis:2018}.
Acknowledging the uncertainty involved in real-time search, the
proposed strategy is to model this uncertainty to enable reasoning
about it explicitly.  For this purpose, probability distributions over
distances to the goal are used to model a belief of several possible
outcomes.  Comparing the distribution that has the lowest expected
value with the alternatives yields a measure of confidence that the
seemingly best action is indeed the optimal choice.  The proposed
lookahead phase performs a search that aims to maximize confidence
that the chosen action is indeed optimal.  The algorithm tries to make
more use of the limited amount of search by gathering more information
about the current choice, instead of just following the lowest
$f$-value.  To select an action, the algorithm makes use of the
probability distributions at the frontier nodes by propagating them up
to the top level using the Cserna backup rule
\cite{cerna:meta:icaps-17}.  This backup rule combines the beliefs of
all successors to compute the belief of the parent taking the
trade-off between safety and optimality of different choices into
account.  After all beliefs have been propagated up, the overall best
action is chosen.

A slightly different approach is called \nancy, first described by
Mitchell \etal in \cite{mitchell:thesis:2018} and in
\cite{ruml:nancy:aaai-19}.  It builds on the idea of modeling the
belief of a state with a probability distribution.  \nancy's lookahead
phase is based on the notion of risk.  Instead of maximizing the
confidence that an action is the best choice, it expands nodes to
minimize the potential regret when it is not.  In the action selection
phase, Nancy backs up the beliefs to the root by assigning the parent
the belief of the one child with the smallest expected value.  This
means that not all successor beliefs are taken into account.  However,
it does have the advantage of being computationally cheaper.
Additionally, keeping a probability distribution instead of a scalar
estimate has turned out to be useful empirically.

When probability distributions are used to model the uncertain belief
about the true distance to the goal, a central question is how these
beliefs are obtained.  Heuristic functions are typically used to
estimate the goal distance during search.  However, they only yield a
single scalar value as an estimate with no further information how
close it may be to the true value.  Methods to map the scalar value to
a distribution typically assume a model of heuristic error and follow
a certain type of probability distribution.  One such method is shown
by Spaniol in \cite{spaniol:thesis:2018}, which makes use of two
heuristic functions during search.  One of them is admissible, the
other inadmissible.  To obtain a distribution. their values are
interpolated uniformly.  Although it is not mentioned explicitly, the
underlying assumption of this approach is that the possible outcomes
are spread uniformly between two heuristic values.

Mitchell proposes a different strategy in \cite{mitchell:thesis:2018}.
The proposed method first corrects the lower bound estimate given by
an admissible heuristic to a non-admissible goal distance estimate
using a method by Thayer, Dionne, and Ruml
\cite{dionne:heuristics:icaps11}.  For this purpose, it estimates the
average single-step error and the number of steps to the goal.  This
corrected heuristic value is mapped to a distribution of possible goal
distances.  The error is assumed to follow a Gaussian distribution
around the corrected heuristic value.  The variance of the
distribution is derived from the difference of the corrected and the
original estimate.

What these methods to obtain beliefs have in common, is that they use
scalar heuristic values as initial estimates of the goal distance and
then map them to probability distributions.  The mappings themselves
are based on assumptions about the error of the initial estimate.
These methods do not yield representative beliefs if the assumptions
used to construct them are violated.  In this thesis we explore a
different approach which gathers data about the heuristic behavior
offline and uses this data during search to construct beliefs.  The
gathered data is not based on generic assumptions and has the
potential to represent different kinds of heuristic behavior.  We
explore how beliefs based on data can lead to more informed decisions
and evaluate the resulting search performance.

When evaluating real-time techniques, their running time plays a
significant role, since the purpose of real-time planning in general
is the design of algorithms able to perform under strict time limits.
However, it is well known that evaluating the running times of
algorithms is inconvenient since the specific time it takes for
certain operations to complete depends on many details, like the
specific hardware and the implementation of the algorithm.  Even with
a fixed hardware and implementation setup, successive runs may give
slightly different results due to the complexity of hardware caches
and system interrupts.  This makes evaluating and comparing new
algorithms significantly more difficult.  In light of this, the
typical methodology for real-time planning has been to embrace the
three-step paradigm and limit the number of iterations allowed in the
lookahead phase.  The expansion bound limits the amount of search the
real-time algorithm can do before having to commit to an action and
serves as an approximation of a time bound.  As a consequence,
real-time algorithms are typically formulated with an expansion bound
for simplicity
\cite{koenig-sun:lsslrta:2009,mitchell:thesis:2018,ruml:nancy:aaai-19,spaniol:thesis:2018}.
However, considering that these algorithms introduce additional
reasoning in the lookahead phase to make more effective use of the
available expansions, comparing their real-time effectiveness becomes
more difficult.  The use of an expansion bound is often acknowledged
as an approximation and certain strategies have been used to make the
evaluation of these strategies more time-sensitive.  Kiesel and Burns
in \cite{kiesel:agq}, and Hernández and Baier in \cite{hernandex:aed}
implement expansion-bounded real-time algorithms and compare them
based on the average time per planning episode.  While this does make
the experiments sensitive to the running time of each algorithm, using
average planning time per episode instead of the maximum breaks the
real-time guarantee since the episodes only comply with the time limit
\emph{on average}.  In \cite{kiesel:agq}, they note that the maximum
time per episode is too pessimistic which is attributed to occasional
outliers caused by operating system interrupts.  Eliminating these
outliers would require careful hardware specific optimizations and the
use of a real-time operating system which would have made the
implementation much more difficult.  Eifler and Fickert in
\cite{eifler:aaai:2019}, use time bounds instead of expansion bounds
in their implementation of a real-time algorithm with abstraction
refinement.  Under the assumption that the time limit allows the
algorithm to finish the backup phase and at least one lookahead
iteration, their algorithm always returns an action before the time
limit.  In \cite{cserna:phd}, Cserna also uses time bounds and takes
additional care in his measurements to account for low-level effects
in the operating system that could cause an algorithm to overshoot its
time bound.

In this thesis we implement \nancy\ under both expansion and time
bounds.  We first introduce it with expansion bounds and show what
needs to be considered when running under a time limit instead.

\section{Contributions}
We design and implement \nancydd, a data-driven variant of the \nancy\
real-time planning algorithm.  We consider two further variants of
\nancydd, extending the data-driven approach in different ways.  We
implement our algorithms under both expansion bounds and strict time
limits.  Evaluating \nancydd\ under an expansion bound, we show that
its additional reasoning often pays off and results in lower solution
cost on average.  We show that \nancydd's overhead can be reduced
significantly and the evaluation under time limits shows that
\nancydd\ is a viable choice in a practical real-time setting.

Part of the work shown in this thesis has been published for AAAI-20
as ``Beliefs We Can Believe In: Replacing Assumptions with Data in
Real-Time Search'' \cite{fickert:gu:staut:aaai20}, to which we
contributed the description of \nancydd\ as well as the implementation
of \nancy\ and \nancydd, which are written on top of an extended
version of Fickert's original real-time planning framework.  In
\nancy\ we make use of a customized belief implementation, initially
based on a version by Mitchell \cite{mitchell:thesis:2018}.

\section{Outline}
In \chapterref{chapter:background} we introduce the planning framework
in the classical form and its real-time variant.  We show the
challenges unique to real-time planning and introduce the \nancy\
algorithm highlighting the methods used to overcome these challenges,
and which assumptions were made in its design.  We follow up with
novel extensions to \nancy.  In \chapterref{chapter:dd} we show
various methods how \nancy\ can be extended in a data-driven way to
overcome some of its assumptions.  In \chapterref{chapter:rt} we
describe an execution strategy for \nancy\ when running under time
bounds.  Relevant implementation details are given in
\chapterref{chapter:implementation}.  In particular we show how
\nancy's unique techniques are implemented to be viable in a real-time
setting.  Finally, the evaluation in \chapterref{chapter:evaluation}
compares different variants of \nancy\ against \lsslrta\ on a set of
planning benchmarks.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
