\chapter{Real-Time Nancy}
\label{chapter:rt}

\nancy\ is ultimately an approach to real-time search.  As mentioned
in \chapterref{chapter:background} however, real-time search
algorithms typically use a bound on the number of expansions as an
approximation of the time limit.  They are typically evaluated under
this expansion bound \cite{mitchell:thesis:2018,spaniol:thesis:2018},
or by comparing mean time per planning episode
\cite{kiesel:agq,hernandex:aed} for each expansion limit.  In this
chapter we examine expansion bounds as an approximation of time bounds
more closely.  We then show how to implement \nancy\
(\algorithmref{alg:nancy}) to make it viable when running under strict
time bounds.

\section{Evaluating Real-Time Planning Algorithms}
\label{section:whytime}
% In the previous sections, the Nancy algorithm has been presented in a
% formulation using a limited number of expansions, as the commonly used
% approximation of a real-time bound.  This simplifies both design and
% implementation of the algorithm.

Real-time planning algorithms are most often implemented using
expansion bounds as an approximation.  This does limited the number of
operations of the algorithm before it has to commit to an action.  It
abstracts from the runtime of each iteration, which is exactly the
main motivation behind this approach, since it eliminates the
scheduling aspect of the real-time algorithm.  When actual time is
considered, the algorithm would need to decide how much time can be
spent on each operation.  Furthermore, any phase taking too much time
would need to be interruptible to guarantee responsiveness.  These
issues are typical for real-time systems and usually lead to a more
complex implementation of the algorithm.

However, by abstracting the running time of real-time algorithms away,
we fail to model certain issues that arise only when running under an
actual time limit.  The first issue is the specificity of the bound.
Expansion bounds only limit the iterations of the lookahead phase.
All other phases are in principle unbounded in their running time.
With time bounds however, an algorithm that performs additional
complex operations after the lookahead, \eg a more involved learning
phase \cite{spaniol:thesis:2018} or abstraction refinement
\cite{eifler:aaai:2019}, would be required to make enough time for it
by reducing its amount of lookahead.

The second issue when using expansion bounds is that it may give more
involved real-time algorithms an advantage.  When only the number of
expansions is counted, it does not matter how much time is required to
do a single expansion.  Additional reasoning in each lookahead
iteration is free, in the sense that there is no limit to it.  This
could be accounted for by measuring the time for a single search
episode when running under a given expansion bound.  As noted by
\cite{kiesel:agq} however, this is not as straightforward as it may
seem, since the mean time per episode may be slightly too generous
while the maximum time is too strict.

The third issue an expansion bound fails to address is the variance of
time needed by a single expansion.  The amount of time spent in a
single expansion depends mainly on the kinds of states that are seen
in the lookahead.  If a state was seen in a previous lookahead
iteration, then the belief associated with it is known already and
just needs to be looked up.  If it is a new state however, it first
needs to be evaluated.  Therefore, depending on the ratio between new
and known states that are encountered in each iteration, a lookahead
phase may overall be able to carry out fewer or more expansions in the
same amount of time.  There is a subtle effect during real-time search
which makes this point significant: In the very first search episodes,
the algorithm has not seen any states yet.  Therefore, for most or all
of the states that are generated, we first need to compute the
heuristic.  After the search has been running for some time however,
more and more states are known from previous lookahead phases already.
This does make a big difference.  Depending on the complexity of the
heuristic that is used, looking up the belief for a known state can be
orders of magnitudes faster than creating a new belief.  When running
with an expansion bound, this results in a decrease of the time spent
in subsequent search episodes.  Under a time bound however, the
algorithm would use the newly available time to increase the amount of
lookahead, as long as there is time left.  Therefore, there is some
potential to drive the frontier slightly further away from the current
state.

\section{Time-Bound Nancy}
\label{section:timebound}

We choose time bounds as the method to evaluate the time overhead of
\nancy.  Running an algorithm under a time limit requires some care
though, and this section describes the relevant considerations to make
\nancy\ real-time viable.

When using a clock on the algorithm as the limit, we lose the
convenience of expansion bounds.  Specifically, we need to actively
schedule all phases of \nancy, since it is not sufficient to put only
the lookahead phase under a time limit.  The backup phase also needs
to check the available time. To guarantee that the deadline is met,
the learning phase needs to be interruptible at each iteration, if not
enough time is left for it to finish.  In the case that the backup
phase runs out of time before the deadline, it carries over to the
next search episode, and is finished before the next lookahead can be
started.

To guarantee that Nancy stays below the time limit, the next action
needs to be chosen as early as possible.  This is achieved with a
simple but significant change in the order of operations.  Instead of
backing up the beliefs after the lookahead, and then deciding which
action to take, \nancy\ switches both phases around and does the
action selection immediately after each lookahead.  This has the
advantage that Nancy can guarantee returning an action in time, before
the learning phase.  This approach is valid since the method to select
an action used by \nancy\ does not depend on the final belief backup.
To select an action, \nancy\ only needs the beliefs at the frontier.
Beliefs of intermediate nodes may still be unaligned at this point.
The final belief backup only needs to happen before the next lookahead
phase, to ensure that the updated beliefs are available.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.96\textwidth]{execution.png}
  \caption{\nancy\ execution strategy}
  \label{fig:nancy:execution}
\end{figure}

\figureref{fig:nancy:execution} shows the progression of \nancy's
execution over time.  Each phase is shown in a different color, and
black vertical bars indicate the time intervals.  \nancy\ starts
execution with the lookahead phase.  The lookahead is however always
stopped early to ensure that there is some time left.

\newpage
To stop the lookahead early enough, \nancy\ estimates how long a
single lookahead iteration is going to take.  Each lookahead iteration
is timed, and the maximum is kept as a worst-case estimate.  The worst
case regarding the execution time of the lookahead occurs when we
generate new states for which we need to compute the heuristic, since
this is the most expensive operation.  Therefore, the worst case
typically happens in the first few iterations of the search, when many
new states are seen for the first time.

After lookahead, \nancy\ first spends the remaining time to select the
action to execute next.  Action selection is then followed by the
learning phase.  Since the learning phase is the last part of the
algorithm, it may execute very close to the deadline, and there are
two cases that can happen during that time.  The first case is that it
finishes early, \ie there is time to spare before the deadline.  Then
\nancy\ starts the next lookahead phase early.  In the second case,
the learning phase does not terminate before the deadline.  Then it is
interrupted before time runs out to ensure that an action is returned
in time.  The next episode then has to start by finishing up the
remainder of the learning phase.

Under the assumption that the time limit allows the lookahead phase to
carry out at least one expansion within the episode, this scheme
always returns the next action to execute before the deadline.  The
requirement of a single expansion is necessary since \nancy\ needs to
expand at least the root node in order to know which actions are even
applicable and to select among them.

\begin{remark}
  \figureref{fig:nancy:execution} is only a schematic view of \nancy's
  execution to show which phase is executed at which point in time.
  The relative size of the individual phases are not to scale.  In
  reality, \nancy\ spends almost all of its time in the lookahead
  phase.  The learning phase is almost two orders of magnitude shorter
  than the lookahead phase, and the time spent in the action selection
  phase is barely even measurable.  This means that the case of having
  to interrupt the learning phase is easily avoided in practice by
  stopping the lookahead phase prematurely, which leaves enough time
  for the learning phase to finish before the deadline.
\end{remark}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main"
%%% End:
