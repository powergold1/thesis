#!/bin/env python3
import os
import sys
import subprocess
import matplotlib.pyplot as plt
from math import sqrt, log

# dots per inch
dpi=200
# font size
fs=max(sqrt(dpi)*0.5, 9)
# line width
lw=0.5
# bar width
bw=0.9

def read_data(f, truncate):
    res = {}
    res_list = []
    max_h = 0
    for line in f:
        if line == "\n":
            break
        vals = line.split(" ")
        h = int(vals[0])
        if h > max_h:
           max_h = h
        num_points = int(vals[1])
        hstars = []
        probs = []
        i = 2
        factor = 1
        if truncate:
            factor = 10
            while i < len(vals):
                cnt = int(vals[i+1])
                i += 2
                logc = log(cnt, 10)
                if logc < factor:
                    factor = logc
            factor = max(1, factor)
            factor = 10 ** int(factor)
            if num_points > 10 ** 4:
                factor = max(100, factor)
        i = 2
        while i < len(vals):
            hstar = int(vals[i])
            cnt = int(vals[i+1])
            if truncate:
                cnt = int(cnt/factor)
                cnt = max(cnt, 1)
            hstars.extend([hstar] * cnt)
            i += 2
        res[h] = hstars
    return (res, max_h)

def read_data2(f):
    res = {}
    res_list = []
    max_h = 0
    for line in f:
        if line == "\n":
            break
        vals = line.split(" ")
        h = int(vals[0])
        if h > max_h:
           max_h = h
        num_points = int(vals[1])
        hstars = []
        cnts = []
        i = 2
        while i < len(vals):
            hstar = int(vals[i])
            cnt = int(vals[i+1])
            hstars.append(hstar)
            cnts.append(cnts)
            i += 2
        res[h] = [hstars,cnts]
    return (res, max_h)

out_files = []
for file_name in sys.argv[1:]:
    with open(file_name, 'r') as f:
        out_dir = file_name[:-4] + "_plots"
        out_file = os.path.join(out_dir, "violin.png")
        out_files.append(out_file)
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        print("reading data from ", file_name)
        data, max_h = read_data(f, True)

        print("making plot")
        positions = []
        plot_list = []
        for i in range(0, max_h+1):
            tmp = data.get(i)
            if tmp != None:
                plot_list.append(tmp)
                positions.append(i)

        widths = [0.8] * len(positions)

        #level_len = 50
        level_len = len(positions)
        num_levels = int((level_len-1+len(positions)) / level_len)
        assert num_levels == 1
        if num_levels == 1:
            level_len = len(positions)
        figscalew = 4
        figscaleh = 2
        figsize=(len(positions) / figscaleh, figscalew * num_levels)

        fig, axs = plt.subplots(nrows=num_levels,
                                ncols=1,
                                figsize=figsize)
        if num_levels == 1:
            axs.tick_params(axis="both", which="major", labelsize=fs)
            axs.tick_params(axis="both", which="minor", labelsize=fs)
        for i in range(0,num_levels):
            if num_levels > 1:
                ax = axs[i]
                ax.tick_params(axis="both", which="major", labelsize=fs)
                ax.tick_params(axis="both", which="minor", labelsize=fs)
            else:
                ax = axs
            ax.set_xlabel("h", fontsize=fs)
            ax.set_ylabel("h* distribution", fontsize=fs)
            start = i * level_len
            end = (i+1) * level_len
            sub_data = plot_list[start:end]
            sub_positions = positions[start:end]
            sub_widths = widths[start:end]
            parts = ax.violinplot(sub_data,
                                  positions=sub_positions,
                                  widths=sub_widths,
                                  showmeans=True)
            for pc in parts['bodies']:
                pc.set_facecolor("#d43f3a")
                pc.set_edgecolor("black")
                pc.set_linewidth(lw)
                pc.set_alpha(1.0)
            meanbar = parts['cmeans']
            meanbar.set_edgecolor("black")
            meanbar.set_linewidth(bw)
            minbar = parts['cmins']
            minbar.set_edgecolor("black")
            minbar.set_linewidth(bw)
            maxbar = parts['cmaxes']
            maxbar.set_edgecolor("black")
            maxbar.set_linewidth(bw)
            mainline = parts['cbars']
            mainline.set_edgecolor("black")
            mainline.set_linewidth(lw)

        #fig.show()
        print("saving figure ", out_file)
        fig.savefig(out_file, bbox_inches="tight", dpi=dpi)
        fig.clf()

# if len(out_files) > 0:
#     print("opening nomacs")
#     p = subprocess.Popen(["nomacs"] + out_files)
#     p.communicate()
