#!/usr/bin/env python
import os
import sys
import json
import matplotlib.pyplot as plt
from scipy import stats
from math import sqrt

# Take the costs from previous experiment run (the most recent only ran nancy-dd again)
aux_costs = "../solution_costs"
lookaheads = [100, 300, 1000]
cost_files = ["transport.txt", "transport_unit.txt", "elevators_unit.txt", "blocks.txt"]
#cost_files = ["transport.txt", "transport_unit.txt", "elevators_unit.txt", "blocks.txt", "refuel_nomystery07.txt"]
#cost_files = ["transport_unit.txt", "elevators_unit.txt", "blocks.txt"]
#cost_files = ["blocks.txt"]
#cost_files = ["transport.txt"]
domains = { file: file[0:file.index(".")] for file in cost_files }
plt.rcParams['figure.figsize'] = (8.0, 8.0)

class SolutionCosts:
    def __init__(self, limit):
        self.ddcosts = {}
        self.othercosts = {}
        self.limit=limit

    def add(self, lookahead, domain, a_costs, b_costs):
        assert len(a_costs) == len(b_costs)
        a_costs,b_costs = self.sanitize_costs(a_costs, b_costs)
        assert len(a_costs) == len(b_costs)
        m1 = self.ddcosts.get(lookahead)
        m2 = self.othercosts.get(lookahead)
        if m1 == None:
            assert m2 == None
            self.ddcosts[lookahead] = {domain: a_costs}
            self.othercosts[lookahead] = {domain: b_costs}
        else:
            m1[domain] = a_costs
            m2[domain] = b_costs

    def sanitize_costs(self, a, b):
        r1 = []
        r2 = []
        limit = self.limit
        for x,y in zip(a,b):
            if x != 0 and y != 0 and ((limit == None) or (limit != None and x < limit and y < limit)):
                r1.append(x)
                r2.append(y)

        return (r1,r2)

    def get(self, lookahead, domain):
        return self.ddcosts[lookahead][domain], self.othercosts[lookahead][domain]

def read_all_costs():
    all_solution_costs = SolutionCosts(limit=400)
    for file_name in cost_files:
        with open(file_name) as f:
            costs = eval(f.read())
            aux_file = os.path.join(aux_costs, file_name)
            if os.path.exists(aux_file):
                with open(aux_file) as f:
                    costs2 = eval(f.read())

        del costs["max"]

        for config, carr in costs.items():
            if len(carr) == 0:
                costs[config] = costs2[config]

        domain = domains[file_name]

        a_algo = "Nancy DD"
        b_algo = "Nancy Non-DD"
        for l in lookaheads:
            a_config = a_algo + "-" + str(l)
            b_config = b_algo + "-" + str(l)
            a_costs = costs[a_config]
            b_costs = costs[b_config]
            all_solution_costs.add(l, domain, a_costs, b_costs)

    return all_solution_costs

def plot(all_solution_costs, use_log_scale):
    for l in lookaheads:
        if use_log_scale:
            plt.xscale('log')
            plt.yscale('log')
        max_cost = 0
        for file, d in domains.items():
            dd_costs, other_costs = all_solution_costs.get(l, d)
            # scatter the other way around, so "points below the line are good"
            plt.scatter(other_costs, dd_costs, label=d, s=10.0)
            plt.ylabel("DD-Nancy cost")
            plt.xlabel("Non-DD-Nancy cost")
            lx, rx = plt.xlim()
            dy, uy = plt.ylim()
            max_cost = max(rx,uy)
            # plt.xlim(0, max_cost)
            # plt.ylim(0, max_cost)
        plt.plot([0,max_cost],[0,max_cost], color="grey")
        plt.legend()
        name = ("log_scatter_" if use_log_scale else "scatter_") + str(l) + ("_zoom" if all_solution_costs.limit != None else "") + ".png"
        plt.savefig(name, bbox_inches="tight", dpi=300)
        plt.clf()

def t_test(all_solution_costs):
    for l in lookaheads:
        nx = 0
        ny = 0
        mx = 0
        my = 0
        mdx = 0
        mdy = 0

        for f, domain in domains.items():
            ddc, oc = all_solution_costs.get(l, domain)

            assert len(ddc) == len(oc)
            nx += len(ddc)
            ny += len(oc)
            # n += nx + ny
            mx += sum(ddc)
            my += sum(oc)

        mx /= nx
        my /= ny

        for f, domain in domains.items():
            ddc, oc = all_solution_costs.get(l, domain)
            for x in ddc:
                diff = x - mx
                mdx += (diff*diff)
            for y in oc:
                diff = y - my
                mdy += (diff*diff)

        stdx2 = mdx/(nx-1)
        stdy2 = mdy/(ny-1)
        sex = stdx2 / nx
        sey = stdy2 / ny
        sed = sqrt(sex + sey)

        t = (mx-my)/sed
        df = nx + ny - 2
        alpha = 0.05
        cv = stats.t.ppf(1.0-alpha,df)
        p = 2*(1-stats.t.cdf(abs(t),df))
        print("lookahead:", l, "t:", t, "p:", p, "cv:", cv, "df:", df)

def t_test_scipy(all_solution_costs):
    for l in lookaheads:
        data1 = []
        data2 = []

        for f, domain in domains.items():
            ddc, oc = all_solution_costs.get(l, domain)
            data1 += ddc
            data2 += oc

        t, p = stats.ttest_ind(data1,data2)
        print("lookahead:", l, "t:", t, "p:", p)
        r, q = stats.ttest_rel(data1,data2)
        print("lookahead:", l, "r:", r, "q:", q)

all_costs = read_all_costs()
# plot(all_costs, False)
# plot(all_costs, True)
# t_test(all_costs)
t_test_scipy(all_costs)
