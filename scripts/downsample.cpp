#include <iostream>
#include <fstream>
#include <cstdlib>

#include <unordered_map>
#include <vector>

#include "read.hpp"

enum Constants
{
 MAX_SAMPLES = 64,
};

struct hstar_dstr_entry {
  long long int hstar_value;
  long mass;
};

struct hstar_dstr {
  long value_count;
  std::vector<hstar_dstr_entry> vals;
};


int main(int argc, char **argv)
{
  constexpr int max_samples = 64;
  constexpr double inv_max_samples = 1.0 / static_cast<double>(max_samples);
  const std::string max_samples_str = std::to_string(max_samples);
  for (int i = 1; i < argc; ++i) {
    std::string file_name(argv[i]);

    hstar_data all_hstar_data = read_hdata(file_name);
    auto const &hdata = all_hstar_data.hdata;
    auto const &hdata_vec = all_hstar_data.hdata_vec;

    // downsample
    std::vector<std::pair<long, hstar_dstr > > hdata_down;
    for (const auto &p : hdata_vec) {
      long h = p.first;
      hstar_data_entry *samples = p.second;

      long const scale = samples->value_count / max_samples;
      hdata_down.emplace_back(h, hstar_dstr{samples->value_count, {}});
      hdata_down.back().second.vals.reserve(max_samples);
      if (scale < 2) {
        for (const auto &[hstar_val, count] : samples->hstar_values) {
          hdata_down.back().second.vals.emplace_back(hstar_dstr_entry{hstar_val, count});
        }
      } else {
        long mass = 0;
        long long int hs_sum = 0;
        for (const auto &[hstar_val, count] : samples->hstar_values) {
          hs_sum += hstar_val * count;
          mass += count;
          if (mass >= scale) {
            // TODO: (hs_sum / mass) should be a double due to
            // averaging but rounding it to an int makes the
            // downsampled data format compatible with the input
            // format (which doesn't need to have doubles in the first
            // place).
            hdata_down.back().second.vals.emplace_back(hstar_dstr_entry{hs_sum / mass, mass});
            mass = 0;
            hs_sum = 0;
          }
        }
      }
    }

    // write downsampled distribution out to a file
    size_t indx = file_name.rfind(".");
    std::string out_name(file_name.substr(0, indx) + "_d" + max_samples_str + ".txt");
    std::ofstream out_file;
    out_file.open(out_name);
    std::cout << out_name << "\n";

    for (const auto &p : hdata_down) {
      out_file << (p.first);
      out_file << (' ');
      out_file << (p.second.value_count);
      // out_file << (' ');
      // out_file << (p.second.max_samples);
      for (const auto &e : p.second.vals) {
        out_file << (' ');
        out_file << (e.hstar_value);
        out_file << (' ');
        out_file << (e.mass);
      }
      out_file << ('\n');
    }
    out_file.close();
  }
  return 0;
}
