#!/usr/bin/env python
import os
import sys
import json
import matplotlib.pyplot as plt
import numpy as np

def read_costs_into(f_name, costs):
    with open(f_name) as f:
        for line in f.readlines():
            l = line.split(": ")
            costs[l[0]].append(int(l[1]))

def with_xs(config_costs):
    res = ([], [])
    i = 0
    for c in config_costs:
        if c != 0:
            res[0].append(i)
            res[1].append(c)
        i += 1
    return res


# colors = {
#     "LSS-LRTA*-100":'#800000',
#     "LSS-LRTA*-300":'#a02020',
#     "LSS-LRTA*-1000":'#f04040',
#     "Nancy DD-100": '#008000',
#     "Nancy DD-300": '#20a020',
#     "Nancy DD-1000": '#40f040',
#     "Nancy Non-DD-100": '#000080',
#     "Nancy Non-DD-300": '#2020a0',
#     "Nancy Non-DD-1000": '#4040f0'
#     }

colors = {
    "LSS-LRTA*-100":'#800000',
    "LSS-LRTA*-300":'#800000',
    "LSS-LRTA*-1000":'#800000',
    "Nancy DD-100": '#008000',
    "Nancy DD-300": '#008000',
    "Nancy DD-1000": '#008000',
    "Nancy Non-DD-100": '#000080',
    "Nancy Non-DD-300": '#000080',
    "Nancy Non-DD-1000": '#000080'
    }

aux_costs = "../solution_costs"

for file_name in sys.argv[1:]:
    with open(file_name) as f:
        costs = eval(f.read())
    with open(os.path.join(aux_costs, file_name)) as f:
        costs2 = eval(f.read())

    lookaheads = [100, 300, 1000]
    print("making plots for", file_name)
    for lookahead in lookaheads:
        for config, config_costs in costs.items():
            if config == "max":
                continue
            if config.startswith("LSS"):
                continue
            if not config.endswith(str(lookahead)):
                continue
            i = 0
            if len(config_costs) == 0:
                config_costs = costs2[config]
            config_costs = with_xs(config_costs)
            plt.rcParams['figure.figsize'] = (0.4 * len(config_costs[1]), 4.8)
            color = colors[config]
            ms = 5.0
            plt.plot(config_costs[0], config_costs[1],color=color, marker='8', linestyle='', markersize=ms, label=config)
            plt.legend()
        # for cost in config_costs:
        #     plt.plot(i, cost)
        #     i += 1

        #plt.show()
        plt.savefig(file_name[:-4] + "_" + str(lookahead) + "_solution_costs.png", bbox_inches="tight", dpi=300)
        plt.clf()

        #     for config, color in colors.items():
        #         plt.plot(i, costs[config], color=color)
        #     i += 1
        # plt.show()
        # #plt.savefig(os.path.join(dir, "solution_costs.png"), bbox_inches="tight", dpi=100)
        # plt.clf()
