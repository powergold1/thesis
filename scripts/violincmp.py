#!/bin/env python3
import os
import sys
import subprocess
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from math import sqrt, log

# dots per inch
dpi=200
# font size
fscale=1.65
fs=fscale*max(sqrt(dpi)*0.5, 9)
# line width
lw=0.5
# bar width
bw=0.9

nicenames = {
    "blocks.txt": "Blocksworld",
    "transport_unit.txt": "Transport (unit)",
    "blocks_l.txt": "Blocksworld (LSS-LRTA*)",
    "blocks_r2_conv.txt": "Blocksworld (half as many instances)",
}

def read_data(f, truncate):
    res = {}
    res_list = []
    max_h = 0
    for line in f:
        if line == "\n":
            break
        vals = line.split(" ")
        h = int(vals[0])
        if h > max_h:
           max_h = h
        num_points = int(vals[1])
        hstars = []
        probs = []
        i = 2
        factor = 1
        if truncate:
            while i < len(vals):
                cnt = int(vals[i+1])
                i += 2
                logc = log(cnt, 10)
                if logc > factor:
                    factor = logc
            factor -= 1
            factor = max(1, factor)
            factor = 10 ** int(factor)
            if num_points > 10 ** 4:
                factor = max(100, factor)
        i = 2
        while i < len(vals):
            hstar = int(vals[i])
            cnt = int(vals[i+1])
            if truncate:
                cnt = int(cnt/factor)
                #cnt = int(log(cnt,2))
                cnt = max(cnt, 1)
            hstars.extend([hstar] * cnt)
            i += 2
        res[h] = hstars
    return (res, max_h)

def read_data2(f):
    res = {}
    res_list = []
    max_h = 0
    for line in f:
        if line == "\n":
            break
        vals = line.split(" ")
        h = int(vals[0])
        if h > max_h:
           max_h = h
        num_points = int(vals[1])
        hstars = []
        cnts = []
        i = 2
        while i < len(vals):
            hstar = int(vals[i])
            cnt = int(vals[i+1])
            hstars.append(hstar)
            cnts.append(cnts)
            i += 2
        res[h] = [hstars,cnts]
    return (res, max_h)

def main():
    if len(sys.argv) != 3:
        print("Gimme 2 arguments")
        sys.exit(1)
    file_name1 = sys.argv[1]
    file_name2 = sys.argv[2]
    out_dir = file_name1[:-4] + "_plots"
    vert=True
    grid=True
    truncate=False
    out_file = os.path.join(out_dir, "vs" + file_name2[:-4] + ("rviolin.png" if not vert else "violin.png"))

    with open(file_name1, 'r') as f:
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)

        print("reading data from ", file_name1)
        data1, max_h1 = read_data(f, truncate)

    with open(file_name2, 'r') as f:
        print("reading data from ", file_name2)
        data2, max_h2 = read_data(f, True)

    print("making plot")
    positions1 = []
    plot_list1 = []
    positions2 = []
    plot_list2 = []
    for i in range(0, max_h1+1):
        tmp = data1.get(i)
        if tmp != None:
            plot_list1.append(tmp)
            positions1.append(i)

    for i in range(0, max_h2+1):
        tmp = data2.get(i)
        if tmp != None:
            plot_list2.append(tmp)
            positions2.append(i)


    #level_len = 50
    level_len = max(len(positions1), len(positions2))
    widths1 = [0.8] * len(positions1)
    widths2 = [0.8] * len(positions2)
    # num_levels = int((level_len-1+len(positions)) / level_len)
    num_levels = 1
    assert num_levels == 1
    if num_levels == 1:
        level_len = max(len(positions1), len(positions2))
    figscalew = 7
    figscaleh = 2
    #figsize=(len(positions) / figscaleh, figscalew * num_levels)
    figsize=(27 / figscaleh, figscalew * num_levels)
    #figsize=(27 / figscaleh, figscalew * num_levels)
    if not vert:
        figsize=(figsize[1],figsize[0])

    fig, axs = plt.subplots(nrows=num_levels,
                            ncols=1,
                            figsize=figsize)
    if num_levels == 1:
        axs.tick_params(axis="both", which="major", labelsize=fs)
        axs.tick_params(axis="both", which="minor", labelsize=fs)
    for i in range(0,num_levels):
        if num_levels > 1:
            ax = axs[i]
            ax.tick_params(axis="both", which="major", labelsize=fs)
            ax.tick_params(axis="both", which="minor", labelsize=fs)
        else:
            ax = axs
        xlabel = "h" if vert else "h* distribution"
        ylabel = "h" if not vert else "h* distribution"
        ax.set_xlabel(xlabel, fontsize=fs)
        ax.set_ylabel(ylabel, fontsize=fs)
        start = i * level_len
        end = (i+1) * level_len

        ax.grid(grid, alpha=0.2, color="#a0a0a0", linewidth=1)
        ax.legend([patches.Patch(color="#d43f3a"), patches.Patch(color="#3034a4")],
                  [nicenames[file_name1], nicenames[file_name2]],
                  loc="upper left")
        parts = ax.violinplot(plot_list1,
                              positions=positions1,
                              widths=widths1,
                              showmeans=True,
                              showextrema=True,
                              vert=vert)
        #ax.set_xlim(left=-2,right=82)
        #ax.set_ylim(bottom=-1,top=27)
        for pc in parts['bodies']:
            pc.set_facecolor("#d43f3a")
            pc.set_edgecolor("black")
            pc.set_linewidth(lw)
            pc.set_alpha(0.7)
        meanbar = parts['cmeans']
        meanbar.set_edgecolor("#802020")
        meanbar.set_linewidth(bw)
        meanbar.set_alpha(0.8)
        minbar = parts['cmins']
        minbar.set_edgecolor("#602020")
        minbar.set_linewidth(bw)
        minbar.set_alpha(0.8)
        maxbar = parts['cmaxes']
        maxbar.set_edgecolor("#602020")
        maxbar.set_linewidth(bw)
        maxbar.set_alpha(0.8)
        mainline = parts['cbars']
        mainline.set_edgecolor("black")
        mainline.set_linewidth(lw)

        parts = ax.violinplot(plot_list2,
                              positions=positions2,
                              widths=widths2,
                              showmeans=True,
                              vert=vert)
        for pc in parts['bodies']:
            pc.set_facecolor("#3034a4")
            pc.set_edgecolor("black")
            pc.set_linewidth(lw)
            pc.set_alpha(0.85)
        meanbar = parts['cmeans']
        meanbar.set_edgecolor("black")
        meanbar.set_linewidth(bw)
        minbar = parts['cmins']
        minbar.set_edgecolor("black")
        minbar.set_linewidth(bw)
        maxbar = parts['cmaxes']
        maxbar.set_edgecolor("black")
        maxbar.set_linewidth(bw)
        mainline = parts['cbars']
        mainline.set_edgecolor("black")
        mainline.set_linewidth(lw)

    #fig.show()
    print("saving figure ", out_file)
    fig.savefig(out_file, bbox_inches="tight", dpi=dpi)
    fig.clf()

main()

# if len(out_files) > 0:
#     print("opening nomacs")
#     p = subprocess.Popen(["nomacs"] + out_files)
#     p.communicate()
