#include "read.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

#include <string>
#include <vector>


int main(int argc, char const *argv[])
{
  for (int i = 1; i < argc; ++i) {
    std::string file_name(argv[i]);
    hstar_data all_hstar_data = read_hdata(file_name);

    auto const &hdata_vec = all_hstar_data.hdata_vec;

    // open file to write variances for each h
    size_t indx = file_name.rfind(".");
    std::string out_name(file_name.substr(0, indx) + "_vrn" + ".txt");
    std::ofstream out_file;
    out_file.open(out_name);
    std::cout << out_name << "\n";

    // compute variance for each h and write it to the file
    for (auto const &p : hdata_vec) {
      long h = p.first;
      hstar_data_entry *entry = p.second;
      long value_count = entry->value_count;
      double d_value_count = static_cast<double>(value_count);
      std::vector<hstar_sample> &samples = entry->hstar_values;

      double exp = 0.0;
      double expsq = 0.0;

      for (auto const &s : samples) {
        double prob = static_cast<double>(s.count) / d_value_count;
        double dh = static_cast<double>(s.hstar_value);
        double a = prob * dh;
        double a2 = a * dh;
        exp += a;
        expsq += a2;
      }

      double variance = expsq - (exp * exp);
      out_file << h << ' ' << exp << ' ' << variance << "\n";
    }

    out_file.close();
  }

  return 0;
}
