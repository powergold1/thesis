#include "read.hpp"
#include <iostream>
#include <sstream>
#include <fstream>


hstar_data read_hdata(std::string const &file_name)
{
  hstar_data res;
  auto &hdata = res.hdata;
  auto &hdata_vec = res.hdata_vec;

  std::ifstream f(file_name);
  std::string line;
  long h, hs;
  long valueCount, hsCount;
  long l = 0;

  while (std::getline(f, line)) {
    std::stringstream ss(line);
    ss >> h;
    ss >> valueCount;

    if (hdata.find(h) != std::end(hdata)) {
      std::cerr << "error: duplicate h from data " << h << std::endl;
      std::exit(1);
    }

    auto &hstar_data_for_h = hdata.emplace(h, hstar_data_entry{valueCount, {}}).first->second.hstar_values;
    while (!ss.eof()) {
      ss >> hs;
      ss >> hsCount;
      hstar_data_for_h.emplace_back(hstar_sample{hs, hsCount});
    }
    hdata_vec.emplace_back(h, &hdata[h]);
  }
  f.close();

  return res;
}
